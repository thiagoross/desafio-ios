//
//  Constants.m
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/28/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "Constants.h"

#define kURLDribbbleAPI @"https://api.dribbble.com"

NSString *const kURLDribbblePopularShots = kURLDribbbleAPI @"/shots/popular";
