//
//  AnimationUtils.h
//  DesafioIOS
//
//  Created by Thiago Rossener on 9/4/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AnimationUtils : NSObject

+ (void)rotateLayerInfinite:(CALayer *)layer;
+ (void)rotateLayerVertically:(CALayer *)layer;

@end
