//
//  Shot.m
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/28/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "Shot.h"

@implementation Shot

- (NSString *)description
{
    return [NSString stringWithFormat:@"{ id:\"%d\", title:%@, description:%@, width:%@, height:%@, likes_count:%@, comments_count:%@, rebounds_count:%@, url:%@, short_url:%@, views_count:%@, rebound_source_id:%@, image_url:%@, image_teaser_url:%@, image_400_url:%@, created_at:%@ }",
                                        self.itemId,
                                        self.title,
                                        self.descriptionText,
                                        self.width,
                                        self.height,
                                        self.likesCount,
                                        self.commentsCount,
                                        self.reboundsCount,
                                        self.url,
                                        self.shortUrl,
                                        self.viewsCount,
                                        self.reboundSourceId,
                                        self.imageUrl,
                                        self.imageTeaserUrl,
                                        self.image400Url,
                                        self.createdAt];
}

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                      @"id":@"itemId",
                                                      @"description": @"descriptionText"
                                                      }];
}

+ (BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
