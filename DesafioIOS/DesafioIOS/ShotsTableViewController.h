//
//  TableViewController.h
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/31/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIImageView+WebCache.h>
#import <SVPullToRefresh/SVPullToRefresh.h>

#import "AnimationUtils.h"
#import "NoMoreUpdatesView.h"
#import "PullToRefreshView.h"
#import "ReleaseToRefreshView.h"
#import "ShotDetailsTableViewController.h"
#import "ShotsUpdater.h"
#import "ShotTableViewCell.h"

@interface ShotsTableViewController : UITableViewController<ShotsUpdaterDelegate,UITableViewDelegate>

@property (strong, nonatomic) ShotsUpdater *shotDAO;
@property (assign, nonatomic) int pageCounter;

@end
