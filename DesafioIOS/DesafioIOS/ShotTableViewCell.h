//
//  ShotTableCellTableViewCell.h
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/31/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShotTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *viewsCount;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
