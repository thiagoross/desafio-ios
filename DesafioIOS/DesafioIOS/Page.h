//
//  Page.h
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/28/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "JSONModel.h"
#import "Shot.h"

@interface Page : JSONModel

@property int page;
@property int perPage;
@property int pages;
@property int total;
@property NSArray *shots;

@end
