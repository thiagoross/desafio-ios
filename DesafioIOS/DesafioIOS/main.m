//
//  main.m
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/28/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
