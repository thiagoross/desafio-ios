//
//  ShotPlayerTableViewCell.h
//  DesafioIOS
//
//  Created by Thiago Rossener on 9/2/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShotPlayerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *playerImage;
@property (weak, nonatomic) IBOutlet UILabel *playerName;

@end
