//
//  Page.m
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/28/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "Page.h"

@implementation Page

- (NSString *)description
{
    return [NSString stringWithFormat:@"{ page:%d, per_page:%d, pages:%d, total:%d, shots:%d }", self.page, self.perPage, self.pages, self.total, self.shots.count];
}

@end
