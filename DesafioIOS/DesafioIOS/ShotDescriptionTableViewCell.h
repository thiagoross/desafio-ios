//
//  ShotDescriptionTableViewCell.h
//  DesafioIOS
//
//  Created by Thiago Rossener on 9/3/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShotDescriptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIWebView *shotDescription;

@end
