//
//  ShotTableCellTableViewCell.m
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/31/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "ShotTableViewCell.h"

@implementation ShotTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
