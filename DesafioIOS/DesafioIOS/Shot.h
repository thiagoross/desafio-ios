//
//  Shot.h
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/28/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "JSONModel.h"
#import "Player.h"

@interface Shot : JSONModel

@property int itemId;
@property NSString *title;
@property NSString *descriptionText;
@property NSNumber *width;
@property NSNumber *height;
@property NSNumber *likesCount;
@property NSNumber *commentsCount;
@property NSNumber *reboundsCount;
@property NSString *url;
@property NSString *shortUrl;
@property NSNumber *viewsCount;
@property NSNumber *reboundSourceId;
@property NSString *imageUrl;
@property NSString *imageTeaserUrl;
@property NSString *image400Url;
@property Player *player;
@property NSDate *createdAt;

@end
