//
//  Player.m
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/28/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "Player.h"

@implementation Player

- (NSString *)description
{
    return [NSString stringWithFormat:@"{ id:\"%d\", name:%@, location:%@, followers_count:%@, draftees_count:%@, likes_count:%@, likes_received_count:%@, comments_count:%@, comments_received_count:%@, rebounds_count:%@, rebounds_received_count:%@, url:%@, avatar_url:%@, username:%@, twitter_screen_name:%@, website_url:%@, drafted_by_player:%@, shots_count:%@, following_count:%@, created_at:%@ }",
            self.itemId,
            self.name,
            self.location,
            self.followersCount,
            self.drafteesCount,
            self.likesCount,
            self.likesReceivedCount,
            self.commentsCount,
            self.commentsReceivedCount,
            self.reboundsCount,
            self.reboundsReceivedCount,
            self.url,
            self.avatarUrl,
            self.username,
            self.twitterScreenName,
            self.websiteUrl,
            self.draftedByPlayerId,
            self.shotsCount,
            self.followingCount,
            self.createdAt];
}

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id":@"itemId",
                                                       @"description": @"descriptionText"
                                                       }];
}

+ (BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
