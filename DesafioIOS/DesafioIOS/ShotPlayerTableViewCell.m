//
//  ShotPlayerTableViewCell.m
//  DesafioIOS
//
//  Created by Thiago Rossener on 9/2/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "ShotPlayerTableViewCell.h"

@implementation ShotPlayerTableViewCell

- (void)awakeFromNib {
    self.playerImage.layer.cornerRadius = self.playerImage.frame.size.width / 2;
    self.playerImage.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
