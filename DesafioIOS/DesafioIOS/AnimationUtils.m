//
//  AnimationUtils.m
//  DesafioIOS
//
//  Created by Thiago Rossener on 9/4/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "AnimationUtils.h"

@implementation AnimationUtils

+ (void)rotateLayerInfinite:(CALayer *)layer
{
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
    rotation.duration = 2.0f; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [layer removeAllAnimations];
    [layer addAnimation:rotation forKey:@"Spin"];
}

+ (void)rotateLayerVertically:(CALayer *)layer
{
    [layer removeAllAnimations];
    
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(M_PI)];
    rotation.duration = 0.2f;
    rotation.repeatCount = 1;
    rotation.fillMode = kCAFillModeForwards;
    rotation.removedOnCompletion = NO;
    
    [layer addAnimation:rotation forKey:@"Spin"];
}

@end
