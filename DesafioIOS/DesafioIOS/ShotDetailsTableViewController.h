//
//  ShotDetailsTableViewController.h
//  DesafioIOS
//
//  Created by Thiago Rossener on 9/2/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIActivityIndicator-for-SDWebImage/UIImageView+UIActivityIndicatorForSDWebImage.h>

#import "Shot.h"
#import "ShotDescriptionTableViewCell.h"
#import "ShotTableViewCell.h"
#import "ShotPlayerTableViewCell.h"

@interface ShotDetailsTableViewController : UITableViewController<UIWebViewDelegate>

@property (strong, nonatomic) Shot *shot;

@end
