//
//  Player.h
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/28/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "JSONModel.h"

@interface Player : JSONModel

@property int itemId;
@property NSString *name;
@property NSString *location;
@property NSNumber *followersCount;
@property NSNumber *drafteesCount;
@property NSNumber *likesCount;
@property NSNumber *likesReceivedCount;
@property NSNumber *commentsCount;
@property NSNumber *commentsReceivedCount;
@property NSNumber *reboundsCount;
@property NSNumber *reboundsReceivedCount;
@property NSString *url;
@property NSString *avatarUrl;
@property NSString *username;
@property NSString *twitterScreenName;
@property NSString *websiteUrl;
@property NSNumber *draftedByPlayerId;
@property NSNumber *shotsCount;
@property NSNumber *followingCount;
@property NSDate *createdAt;

@end
