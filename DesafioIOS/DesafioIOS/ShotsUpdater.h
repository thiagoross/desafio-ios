//
//  ShotDAO.h
//  DesafioIOS
//
//  Created by Thiago Rossener on 8/28/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

#import "Constants.h"
#import "Page.h"
#import "Shot.h"

typedef enum updatePosition {
    UPDATE_ON_TOP,
    UPDATE_ON_BOTTOM
} UpdatePosition;

@protocol ShotsUpdaterDelegate <NSObject>
- (void)updateOnTopShots:(NSArray *)shots withStatus:(int)status;
- (void)updateOnBottomShots:(NSArray *)shots withStatus:(int)status;
@end

@interface ShotsUpdater : NSObject

@property Page *currentPage;
@property NSMutableArray *shots;

+ (id)sharedInstance;
- (void)findShotsForPage:(int)pageNumber sender:(id<ShotsUpdaterDelegate>)sender updateOn:(UpdatePosition)position;

@end
