//
//  ReleaseToRefreshView.m
//  DesafioIOS
//
//  Created by Thiago Rossener on 9/4/15.
//  Copyright © 2015 Rossener. All rights reserved.
//

#import "ReleaseToRefreshView.h"

@implementation ReleaseToRefreshView

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [AnimationUtils rotateLayerVertically:self.arrow.layer];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
